<?php

namespace Drupal\vsauce_sticky_popup\Entity;

/**
 * @file
 * Contains \Drupal\vsauce_sticky_popup\Entity\VstickyPopupConfigEntity.
 */

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface VstickyPopupConfigEntityInterface.
 *
 * @package Drupal\vsauce_sticky_popup\Entity
 */
interface VstickyPopupConfigEntityInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
